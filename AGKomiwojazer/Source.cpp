﻿#define _USE_MATH_DEFINES
#include <iostream>
#include<ctime>
#include "gl/freeglut.h"
#include <stdlib.h>
#include <cstdio>
#include<cmath>
#include <fstream>
#include <string>
//#include <algorithm>
#include <conio.h>

using namespace std;

class Miasto
{
public:
	int x;
	int y;
	Miasto();
	~Miasto();

private:

};

Miasto::Miasto()
{
}

Miasto::~Miasto()
{
}

class Trasa
{
public:
	int *miasta;
	double odleglosc;
	Trasa();
	~Trasa();

private:

};

Trasa::Trasa()
{
}

Trasa::~Trasa()
{
}

const int liczbaOsobnikow = 10000;
const int liczbaPowtorzen = 100;
int numerPopulacji = 0;
const int liczbaMiast = 50;
const int miastoStartowe = 0;
int najkrucej = 0;
bool done = false;
Trasa populacja[liczbaOsobnikow];
Trasa rodzice[liczbaOsobnikow];
Miasto listaMiast[liczbaMiast];
int najlepszy[liczbaMiast];
double best=9999999999;
void reshape(int w, int h)
{
	GLdouble xo = -0.0f, x1 = 1000.0f, yo = -0.0f, y1 = 1000.0f, zo = -1.0f, z1 = 1.0f;
	GLdouble q = (GLdouble)w / h;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h)
	{
		glOrtho(xo, x1, yo / q, y1 / q, zo, z1);
	}
	else
	{
		glOrtho(xo*q, x1*q, yo, y1, zo, z1);
	}
	glMatrixMode(GL_MODELVIEW);
}

const GLint n = 4;
GLfloat point[liczbaMiast][3];
GLfloat lines[liczbaPowtorzen][liczbaMiast][3];
GLfloat bestLines[liczbaPowtorzen][liczbaMiast][3];
void drawBitmapText(char *string, float x, float y, float z)
{
	char *c;
	glRasterPos3f(x, y, z);

	for (c = string; *c != NULL; c++)
	{
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *c);
	}
}


void dodajMiasta(string path, bool losuj = false)
{
	if (losuj == false)
	{
		fstream bla;
		bla.open(path);
		if (bla.is_open())
		{
			double a, b;
			for (int i = 0; i < liczbaMiast; i++)
			{
				bla >> listaMiast[i].x >> listaMiast[i].y;
			}
			bla.close();
		}
	}
	if (losuj == true)
	{
		for (int i = 0; i < liczbaMiast; i++)
		{
			listaMiast[i].x = rand() % 1000;
			listaMiast[i].y = rand() % 1000;
			for (int j = 0; j < i; j++)
			{
				if ((listaMiast[i].x == listaMiast[j].x) && (listaMiast[i].y == listaMiast[j].y))
				{
					i--;
					break;
				}
			}
		}
		ofstream myfile;
		myfile.open(path);
		for (int i = 0; i < liczbaMiast; i++)
		{
			myfile << listaMiast[i].x << "\t" << listaMiast[i].y << endl;
		}
		myfile.close();
	}
}
double sumaOdleglosci(Trasa os)
{
	double result = 0;
	for (int i = 0; i < liczbaMiast - 1; i++)
	{
		result += sqrt(pow(listaMiast[os.miasta[i]].x - listaMiast[os.miasta[i + 1]].x, 2) + pow(listaMiast[os.miasta[i]].y - listaMiast[os.miasta[i + 1]].y, 2));
	}
	result += sqrt(pow(listaMiast[os.miasta[liczbaMiast - 1]].x - listaMiast[os.miasta[miastoStartowe]].x, 2) + pow(listaMiast[os.miasta[liczbaMiast - 1]].y - listaMiast[os.miasta[miastoStartowe]].y, 2));
	return result;
}

void losuj()
{

	int i = 0;
	do
	{
		delete[] populacja[i].miasta;
		populacja[i].miasta = new int[liczbaMiast];
		int tmp[liczbaMiast];
		tmp[0] = miastoStartowe;
		for (int j = 1; j < liczbaMiast; j++)
		{
			int rTmp = rand() % liczbaMiast;
			tmp[j] = rTmp;
			for (int k = 0; k < j; k++)
			{
				if (tmp[k] == rTmp)
				{
					j--;
					break;
				}
			}

		}

		for (int j = 0; j < liczbaMiast; j++)
		{
			populacja[i].miasta[j] = tmp[j];

		}

		i++;

	} while (i != liczbaOsobnikow);
}


void selekcja()
{
	/*double suma = 0;
	double min = 9999999;
		for (int i = 0; i < liczbaOsobnikow; i++)
		{
			for (int j = 0; j < liczbaOsobnikow - 1; j++)
			{
				if (populacja[j].odleglosc > populacja[j + 1].odleglosc)
					swap(populacja[j], populacja[j + 1]);
			}
		}
		for (int i = 0; i < liczbaOsobnikow; i++)
		{
			populacja[i].odleglosc = sumaOdleglosci(populacja[i]);
			suma += populacja[i].odleglosc;
			if (populacja[i].odleglosc< min)
				min = populacja[i].odleglosc;
		}

		int j = 0;
		for (int i = 0; i < liczbaOsobnikow; i++)
		{
			if (j == liczbaOsobnikow)
				j = 0;
			double r = (rand() % (int(suma) * 1000)) / 1000;
			if (r <= ((min - populacja[j].odleglosc + 1 / suma + 1) * 100))
			{
				rodzice[i] = populacja[j];

			}
			else
			{
				i--;
			}
			j++;

		}*/

	int turniej[10];

	for (int i = 0; i < liczbaOsobnikow; i++)
		populacja[i].odleglosc = sumaOdleglosci(populacja[i]);

	for (int i = 0; i < liczbaOsobnikow; i++)
	{
		int najlepszy = 0;
		for (int j = 0; j < 10; j++)
		{
			turniej[j] = rand() % liczbaOsobnikow;
		}
		for (int j = 0; j < 10; j++)
		{
			if (populacja[turniej[j]].odleglosc < populacja[najlepszy].odleglosc)
				najlepszy = turniej[j]; ;
		}
		int tmp[liczbaMiast];
		for (int j = 0; j < liczbaMiast; j++)
		{
			tmp[j] = populacja[najlepszy].miasta[j];
		}
		delete[]rodzice[i].miasta;
		rodzice[i].miasta = new int[liczbaMiast];
		for (int j = 0; j < liczbaMiast; j++)
		{

			rodzice[i].miasta[j] = tmp[j];
		}
		//rodzice[i] = populacja[najlepszy];
	}

}
void krzyzowanie()
{

	for (int i = 0; i < liczbaOsobnikow; i++)
	{
		int a = i, b;
		if (i == liczbaOsobnikow - 1)
			b = 0;
		else
			b = i + 1;
		int miejsce = rand() % (liczbaMiast - 2) + 1;
		int m1 = miejsce;
		miejsce = rand() % (liczbaMiast - 2) + 1;
		int m2 = miejsce;

		for (int j = 0; j < liczbaMiast; j++)
		{
			populacja[a].miasta[j] = -1;
		}
		//delete []populacja[a].miasta;
		//populacja[a].miasta = new int[liczbaMiast];
		int tmp[liczbaMiast];
		if (m1 > m2)
			swap(m1, m2);


		for (int j = m1; j < m2; j++)
		{
			tmp[j] = populacja[a].miasta[j] = rodzice[a].miasta[j];
		}
		int m = 0;
		for (int j = 0; j < liczbaMiast; j++)
		{
			if ((find(begin(tmp), end(tmp), rodzice[b].miasta[j]) != end(tmp)) == false)
			{
				if (populacja[a].miasta[m] < 0)
				{
					populacja[a].miasta[m] = rodzice[b].miasta[j];
					m++;
				}
				else
				{
					m++;
					j--;
				}
			}
		}
		fill_n(tmp, liczbaMiast, -1);

		populacja[a].odleglosc = sumaOdleglosci(populacja[a]);
	}
}
void mutacja()
{

	for (int i = 0; i < liczbaOsobnikow; i++)
	{
		if (rand() % 100 + 1 <= 5)
		{
			int gen = rand() % (liczbaMiast - 1) + 1;
			int gen2 = rand() % (liczbaMiast - 1) + 1;
			int tmp = populacja[i].miasta[gen];
			populacja[i].miasta[gen] = populacja[i].miasta[gen2];
			populacja[i].miasta[gen2] = tmp;
		}


		populacja[i].odleglosc = sumaOdleglosci(populacja[i]);

	}
}
void inwersja()
{

	for (int i = 0; i < liczbaOsobnikow; i++)
	{
		if (rand() % 100 + 1 <= 10)
		{
			int gen = rand() % (liczbaMiast - 2) + 1;
			//int gen2 = rand() % (liczbaMiast - 1) + 1;
			int tmp = populacja[i].miasta[gen];
			populacja[i].miasta[gen] = populacja[i].miasta[gen + 1];
			populacja[i].miasta[gen + 1] = tmp;
		}
		populacja[i].odleglosc = sumaOdleglosci(populacja[i]);
	}
}


void render(void)
{
	if (done == false)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clean the screen and the depth buffer
		glLoadIdentity();

		glClearColor(0.0f, 0.2f, 0.4f, 1.0f); glEnable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}
	glColor3f(1.0f, 0.0f, 0.0f); glPointSize(10.0f);
	glBegin(GL_POINTS);
	glVertex3fv(point[0]);

	glEnd();
	glColor3f(0.0f, 0.0f, 1.0f); glPointSize(7.0f);
	glBegin(GL_POINTS);
	for (GLint i = 1; i < liczbaMiast; i++)
	{
		glVertex3fv(point[i]);
	}
	glEnd();

	glColor3f(1.0f, 0.0f, 1.0f);
	for (GLint i = 0; i < liczbaMiast; i++)
	{
		int x = point[i][0];
		int y = point[i][1];
		string s = to_string(i + 1) + ": (x=" + to_string(x) + ", y=" + to_string(y) + ")";
		char * c = new char[s.length()];
		for (int k = 0; k < s.length(); k++)
		{
			c[k] = s[k];
		}
		drawBitmapText(c, point[i][0] - 20, point[i][1] + 10, 0);
		delete[] c;
	}
	string s = to_string(numerPopulacji);
	char * c = new char[s.length()];
	for (int k = 0; k < s.length(); k++)
	{
		c[k] = s[k];
	}
	drawBitmapText(c, 0, 0, 0);
	delete[] c;

	if (done == false)
	{

		glColor3f(0.5f, 1.0f, 0.0f); glLineWidth(5.0f);


		glBegin(GL_LINE_STRIP);
		for (GLint i = 0; i < liczbaMiast; i++) {
			glVertex3fv(lines[numerPopulacji - 1][i]); Sleep(1);
		}
	}
	if (numerPopulacji != liczbaPowtorzen)
	{
		numerPopulacji++;
		//done = true;
	}
	glEnd();


	//glutSwapBuffers();
	glFlush();

}

void animacja(int argc, char ** argv)
{

	for (int i = 0; i < liczbaMiast; i++)
	{
		point[i][0] = (GLfloat)listaMiast[i].x;

		point[i][1] = (GLfloat)listaMiast[i].y;
		point[i][2] = 0.0f;
	}
	numerPopulacji = 0;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1000, 900);
	glutCreateWindow(" Okno   renderowania   OpenGL ");
	glutReshapeFunc(reshape);
	glutDisplayFunc(render);
	glutIdleFunc(render);
	glutMainLoop();

}
void mainAG()
{
	for (int k = 0; k < liczbaPowtorzen; k++)
	{
		selekcja();

		krzyzowanie();
		mutacja();
		inwersja();

		for (int i = 0; i < liczbaOsobnikow; i++)
		{

			if (populacja[i].odleglosc < populacja[najkrucej].odleglosc)
				najkrucej = i;
		}

		for (int i = 0; i < liczbaMiast; i++)
		{
			lines[k][i][0] = (GLfloat)listaMiast[populacja[najkrucej].miasta[i]].x;

			lines[k][i][1] = (GLfloat)listaMiast[populacja[najkrucej].miasta[i]].y;
			lines[k][i][2] = 0.0f;
		}

		numerPopulacji++;
	}
}

int main(int argc, char ** argv)
{
	cout << "Program do rozwiazywania problemu komiwojarzera" << endl;
	cout << "| 1-auto | 2-manual | 3-animacja | 4-wyczysc | 5-wyjscie |" << endl;
	srand(time(NULL));
	
	bool exit = false;
	do {


		int x;
		cin >> x;
		switch (x)
		{
		case 1:
		{
			int numer = 1;
			do
			{
				cout << endl<<"Numer kolekji: " << numer++ << endl;
				dodajMiasta("example2.txt");
				losuj();

				mainAG();

				
				cout << "Trasa: ";
				for (int j = 0; j < liczbaMiast; j++)
				{
					
					cout << populacja[najkrucej].miasta[j] + 1 << "->";
				}
				cout << populacja[najkrucej].miasta[miastoStartowe] + 1;
				
				cout << endl << "Suma odleglosci: " << populacja[najkrucej].odleglosc << endl;
				if (populacja[najkrucej].odleglosc < best)
				{
					best = populacja[najkrucej].odleglosc;
					for (int i = 0; i < liczbaMiast; i++)
					{
						najlepszy[i] = populacja[najkrucej].miasta[i];
					}
					for (int k = 0; k < liczbaPowtorzen; k++)
					{
						for (int i = 0; i < liczbaMiast; i++)
						{
							bestLines[k][i][0] = lines[k][i][0];

							bestLines[k][i][1] = lines[k][i][1];
							bestLines[k][i][2] = 0.0f;
						}
					}
				}
				if (_kbhit())
					break;
			}while (true);
			break;
		}
		case 2:
		{

			dodajMiasta("example3.txt", true);
			losuj();

			mainAG();

			//najlepszy += "Trasa: ";
			cout << "Trasa: ";
			for (int j = 0; j < liczbaMiast; j++)
			{
				//najlepszy += (populacja[najkrucej].miasta[j] + 1) + "->";
				cout << populacja[najkrucej].miasta[j] + 1 << "->";
			}
			cout << populacja[najkrucej].miasta[miastoStartowe] + 1;
			//najlepszy += "Suma odleglosci: ";
			//najlepszy += populacja[najkrucej].odleglosc;
			cout << endl << "Suma odleglosci: " << populacja[najkrucej].odleglosc << endl;
			break;
		}
		case 3:
		{
			animacja(argc, argv);
			break;
		}
		case 4:
		{
			system("cls");
			cout << "Program do rozwiazywania problemu komiwojarzera" << endl;
			cout << "| 1-auto | 2-manual | 3-animacja | 4-wyczysc | 5-wyjscie |" << endl;
			
			cout << "Trasa: ";
			for (int j = 0; j < liczbaMiast; j++)
			{

				cout << najlepszy[j] + 1 << "->";
			}
			cout << najlepszy[0] + 1;
			cout << endl << "Suma odleglosci: " << best << endl;
			for (int k = 0; k < liczbaPowtorzen; k++)
			{
				for (int i = 0; i < liczbaMiast; i++)
				{
					lines[k][i][0] = bestLines[k][i][0];

					lines[k][i][1] = bestLines[k][i][1];
					lines[k][i][2] = 0.0f;
				}
			}
			animacja(argc, argv);
			break;
		}

		case 5:
		default:
		{
			cout << "Koniec programu";
			exit = true;

			break;
		}

		}

	} while (!exit);
	system("pause");
	return 0;
}